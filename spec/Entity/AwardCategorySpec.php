<?php

namespace spec\App\Entity;

use App\Entity\AwardCategory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AwardCategorySpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(AwardCategory::class);
    }

    public function let()
    {
        $this->beConstructedThrough('named', ['Best in Show']);
    }

    public function it_should_have_an_id()
    {
        $this->id()->shouldBeString();
    }

    public function it_should_have_a_name()
    {
        $this->name()->shouldReturn('Best in Show');
    }

    public function it_should_now_when_it_was_created()
    {
        $this->created()->shouldBeAnInstanceOf(\DateTime::class);
    }
}
