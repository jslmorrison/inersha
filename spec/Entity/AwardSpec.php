<?php

namespace spec\App\Entity;

use App\Entity\Award;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AwardSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(Award::class);
    }

    public function let()
    {
        $this->beConstructedThrough('named', ['AwardName']);
    }

    public function it_should_have_a_uid()
    {
        $this->id()->shouldBeString();
    }

    public function it_should_have_a_name()
    {
        $this->name()->shouldReturn('AwardName');
    }

    public function it_should_know_when_it_was_created()
    {
        $this->created()->shouldBeAnInstanceOf(\DateTime::class);
    }

    public function it_should_be_able_to_transform_to_array()
    {
        $this->toArray($this)->shouldBeArray();
    }

    public function it_should_be_able_to_transform_from_array()
    {
        $entity = $this->getWrappedObject();
        $data = $this->toArray($entity);
        $this->toEntity($data)->shouldBeAnInstanceOf(Award::class);
    }
}
