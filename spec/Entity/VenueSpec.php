<?php

namespace spec\App\Entity;

use App\Entity\Venue;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class VenueSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(Venue::class);
    }

    public function let()
    {
        $this->beConstructedThrough(
            'nameAndAddress',
            [
                'Venue name',
                '123 Downure Street',
                'Yourtown',
                'uk1 1uk',
                'GB'
            ]
        );
    }

    public function it_should_have_an_id()
    {
        $this->id()->shouldBeString();
    }

    public function it_should_have_a_name()
    {
        $this->name()->shouldReturn('Venue name');
    }

    public function it_should_have_address()
    {
        $this->address()->shouldReturn('123 Downure Street');
    }

    public function it_should_have_a_town()
    {
        $this->town()->shouldReturn('Yourtown');
    }

    public function it_should_have_a_postcode()
    {
        $this->postcode()->shouldReturn('uk1 1uk');
    }

    public function it_should_have_a_country()
    {
        $this->country()->shouldReturn('GB');
    }
}
