<?php

namespace spec\App\Entity;

use App\Entity\AwardEvent;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use App\Entity\Award;
use App\Entity\Venue;
use Doctrine\Common\Collections\ArrayCollection;

class AwardEventSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(AwardEvent::class);
    }

    public function let()
    {
        $award = Award::named('Person of the Year');
        $venue = Venue::nameAndAddress(
            'venue 1',
            '123 downure street',
            'downtown',
            'uk1 1uk',
            'GB'
        );
        $this->beConstructedThrough(
            'create',
            [
                'POTY 2020',
                $award,
                $venue,
                new \DateTime(),
                new \DateTime()]
        );
    }

    public function it_should_have_an_id()
    {
        $this->id()->shouldBeString();
    }

    public function it_should_have_a_name()
    {
        $this->name()->shouldReturn('POTY 2020');
    }

    public function it_should_have_a_start_date()
    {
        $this->startDate()->shouldBeAnInstanceOf(\DateTime::class);
    }

    public function it_should_have_an_end_date()
    {
        $this->endDate()->shouldBeAnInstanceOf(\DateTime::class);
    }

    public function it_should_have_assocaited_award()
    {
        $this->award()->shouldBeAnInstanceOf(Award::class);
    }

    public function it_should_have_associated_venue()
    {
        $this->venue()->shouldBeAnInstanceOf(Venue::class);
    }

    public function it_should_have_a_collection_of_categories()
    {
        $this->categories()->shouldBeAnInstanceOf(ArrayCollection::class);
    }
    public function it_should_be_able_to_transform_to_array()
    {
        $this->toArray($this)->shouldBeArray();
    }

    public function it_should_be_able_to_transform_from_array()
    {
        $entity = $this->getWrappedObject();
        $data = $this->toArray($entity);
        $this->toEntity($data)->shouldBeAnInstanceOf(AwardEvent::class);
    }
}
