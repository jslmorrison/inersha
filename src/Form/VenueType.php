<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;

/**
 * Class VenueType
 *
 * @package App\Form
 */
class VenueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            [
                'constraints' => [
                    new NotBlank()
                ]
            ]
        )->add(
            'address',
            TextType::class,
            [
                'constraints' => [
                    new NotBlank()
                ]
            ]
        )->add(
            'town',
            TextType::class,
            [
                'constraints' => [
                    new NotBlank()
                ]
            ]
        )->add(
            'postCode',
            TextType::class,
            [
                'constraints' => [
                    new NotBlank()
                ]
            ]
        )->add(
            'country',
            CountryType::class,
            [
            ]
        );
    }
}
