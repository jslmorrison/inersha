<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Repository\AwardRepository;
use App\Repository\VenueRepository;
use App\Repository\AwardCategoryRepository;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class AwardEventType extends AbstractType
{
    private $awardRepository;
    private $venueRepository;
    private $categoryRepository;

    public function __construct(
        AwardRepository $awardRepository,
        VenueRepository $venueRepository,
        AwardCategoryRepository $categoryRepository
    ) {
        $this->awardRepository = $awardRepository;
        $this->venueRepository = $venueRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $awardChoices = $this->awardRepository
            ->findAllAsChoices();
        $venueChoices = $this->venueRepository
            ->findAllAsChoices();
        $categoryChoices = $this->categoryRepository
            ->findAllAsChoices();
        $builder->add(
            'award',
            ChoiceType::class,
            [
                'choices' => $awardChoices
            ]
        )->add(
            'name',
            TextType::class,
            [
                'constraints' => [
                    new NotBlank()
                ]
            ]
        )->add(
            'venue',
            ChoiceType::class,
            [
                'choices' => $venueChoices
            ]
        )->add(
            'startDate',
            DateTimeType::class,
            []
        )->add(
            'endDate',
            DateTimeType::class,
            []
        )->add(
            'categories',
            ChoiceType::class,
            [
                'choices' =>  $categoryChoices,
                'expanded' => true,
                'multiple' => true
            ]
        );
    }
}
