<?php

namespace App\Entity;

use Ramsey\Uuid\Uuid;

/**
 * Class Award
 *
 * @package App\Entity
 */
class Award
{
    private $id;
    private $name;
    private $created;

    private function __construct()
    {
    }

    public static function named(string $awardName): self
    {
        $award = new Award();
        $award->name = $awardName;
        $award->id = Uuid::uuid4()->toString();
        $award->created = new \DateTime();

        return $award;
    }

    public static function toArray(Award $award): array
    {
        return [
            'id' => $award->id(),
            'name' => $award->name(),
            'created' => $award->created()
        ];
    }

    public static function toEntity(array $data): self
    {
        $award = new Award();
        $award->id = $data['id'];
        $award->name = $data['name'];
        $award->created = $data['created'];

        return $award;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function created(): \DateTime
    {
        return $this->created;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
