<?php

namespace App\Entity;

use Ramsey\Uuid\Uuid;

/**
 * Class Venue
 *
 * @package App\Entity
 */
class Venue
{
    private $id;
    private $name;
    private $address;
    private $town;
    private $postCode;
    private $country;

    private function __construct()
    {
    }
    
    // todo - change this to namedAndAddressed
    public static function nameAndAddress(
        string $name,
        string $address,
        string $town,
        string $postCode,
        string $country
    ) {
        $venue = new Venue();
        $venue->id = Uuid::uuid4()->toString();
        $venue->name= $name;
        $venue->address = $address;
        $venue->town = $town;
        $venue->postCode = $postCode;
        $venue->country = $country;

        return $venue;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function address(): string
    {
        return $this->address;
    }

    public function town(): string
    {
        return $this->town;
    }

    public function postcode(): string
    {
        return $this->postCode;
    }

    public function country(): string
    {
        return $this->country;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
