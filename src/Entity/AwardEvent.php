<?php

namespace App\Entity;

use Ramsey\Uuid\Uuid;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class AwardEvemt
 *
 * @package App\Entity
 */
class AwardEvent
{
    private $id;
    private $name;
    private $award;
    private $venue;
    private $startDate;
    private $endDate;
    private $categories;

    private function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    public static function create(
        string $name,
        Award $award,
        Venue $venue,
        \DateTime $startDate,
        \DateTime $endDate,
        Collection $categories
    ): self {
        $awardEvent = new AwardEvent();
        $awardEvent->id = Uuid::uuid4()->toString();
        $awardEvent->name = $name;
        $awardEvent->award = $award;
        $awardEvent->venue = $venue;
        $awardEvent->startDate = $startDate;
        $awardEvent->endDate = $endDate;
        $awardEvent->categories = $categories;

        return $awardEvent;
    }

    public static function toArray(AwardEvent $awardEvent): array
    {
        foreach ($awardEvent->categories()->toArray() as $category) {
            $categories[] = $category->id();
        }
        return [
            'id' => $awardEvent->id(),
            'name' => $awardEvent->name(),
            'award' => $awardEvent->award(),
            'venue' => $awardEvent->venue(),
            'startDate' => $awardEvent->startDate(),
            'endDate' => $awardEvent->endDate(),
            'categories' => $categories
        ];
    }

    public static function toEntity(array $data): self
    {
        $awardEvent = new AwardEvent();
        $awardEvent->id = $data['id'];
        $awardEvent->name = $data['name'];
        $awardEvent->award = $data['award'];
        $awardEvent->venue = $data['venue'];
        $awardEvent->startDate = $data['startDate'];
        $awardEvent->endDate = $data['endDate'];
        $awardEvent->categories = $data['categories'];

        return $awardEvent;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function startDate(): \DateTime
    {
        return $this->startDate;
    }

    public function endDate(): \DateTime
    {
        return $this->endDate;
    }

    public function award(): Award
    {
        return $this->award;
    }

    public function venue(): Venue
    {
        return $this->venue;
    }

    public function categories(): Collection
    {
        return $this->categories;
    }
}
