<?php

namespace App\Entity;

use App\Entity\Award;
use Ramsey\Uuid\Uuid;

/**
 * Class AwardCategory
 *
 * @package App\Entity
 */
class AwardCategory
{
    private $id;
    private $name;
    private $created;

    private function __construct()
    {
    }

    public static function named(string $name): self
    {
        $awardCategory = new AwardCategory();
        $awardCategory->name = $name;
        $awardCategory->id = Uuid::uuid4()->toString();
        $awardCategory->created = new \DateTime();

        return $awardCategory;
    }

    public static function toArray(AwardCategory $awardCategory): array
    {
        return [
            'id' => $awardCategory->id(),
            'name' => $awardCategory->name(),
            'created' => $awardCategory->created()
        ];
    }

    public static function toEntity(array $data): self
    {
        $awardCategory = new AwardCategory();
        $awardCategory->id = $data['id'];
        $awardCategory->name = $data['name'];
        $awardCategory->created = $data['created'];

        return $awardCategory;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function created(): \DateTime
    {
        return $this->created;
    }
}
