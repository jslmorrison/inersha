<?php

namespace App\MessageHandler;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Repository\AwardRepository;
use App\Message\EditAward;

class EditAwardHandler implements MessageHandlerInterface
{
    private $repository;

    public function __construct(AwardRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(EditAward $message)
    {
        $award = $message->award();
        $this->repository
            ->save($award);
    }
}
