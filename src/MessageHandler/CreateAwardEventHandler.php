<?php

namespace App\MessageHandler;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Message\CreateAwardEvent;
use App\Repository\AwardEventDoctrineRepository;
use App\Entity\AwardEvent;
use App\Repository\AwardRepository;
use App\Entity\Venue;
use App\Repository\VenueRepository;
use App\Repository\AwardCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class CreateAwardEventHandler
 *
 * @package App\MessageHandler
 */
class CreateAwardEventHandler implements MessageHandlerInterface
{
    private $repository;
    private $awardRepository;
    private $venueRepository;
    private $awardCategoryRepository;

    public function __construct(
        AwardEventDoctrineRepository $repository,
        AwardRepository $awardRepository,
        VenueRepository $venueRepository,
        AwardCategoryRepository $awardCategoryRepository
    ) {
        $this->repository = $repository;
        $this->awardRepository = $awardRepository;
        $this->venueRepository = $venueRepository;
        $this->awardCategoryRepository = $awardCategoryRepository;
    }

    public function __invoke(CreateAwardEvent $message)
    {
        $info = $message->info();
        $award = $this->awardRepository
            ->find($info['award']);
        $venue = $this->venueRepository
            ->find($info['venue']);
        $categories = $this->awardCategoryRepository
            ->findMultipleById($info['categories']);
        $awardEvent = AwardEvent::create(
            $info['name'],
            $award,
            $venue,
            $info['startDate'],
            $info['endDate'],
            new ArrayCollection($categories)
        );
        $this->repository
            ->save($awardEvent);
    }
}
