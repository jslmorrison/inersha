<?php

namespace App\MessageHandler;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Message\EditAwardCategory;
use App\Repository\AwardCategoryRepository;
use App\Entity\AwardCategory;

/**
 * Class EditAwardCategoryHandler
 *
 * @package App\MessageHandler
 */
class EditAwardCategoryHandler implements MessageHandlerInterface
{
    private $awardCategoryRepository;

    public function __construct(AwardCategoryRepository $awardCategoryRepository)
    {
        $this->awardCategoryRepository = $awardCategoryRepository;
    }

    public function __invoke(EditAwardCategory $message)
    {
        $awardCategory = AwardCategory::toEntity($message->awardCategory());
        $this->awardCategoryRepository
            ->save($awardCategory);
    }
}
