<?php

namespace App\MessageHandler;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Message\CreateAwardCategory;
use App\Repository\AwardCategoryRepository;
use Psr\Log\LoggerInterface;
use App\Entity\AwardCategory;

class CreateAwardCategoryHandler implements MessageHandlerInterface
{
    private $repository;
    private $logger;

    public function __construct(
        AwardCategoryRepository $repository,
        LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    public function __invoke(CreateAwardCategory $message)
    {
        $awardCategory = AwardCategory::named($message->categoryName());
        $this->repository
            ->save($awardCategory);
        $this->logger
            ->info("Created award category: ".$message->categoryName());
    }
}
