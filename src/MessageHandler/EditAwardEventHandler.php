<?php

namespace App\MessageHandler;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Repository\AwardEventRepository;
use Psr\Log\LoggerInterface;
use App\Message\EditAwardEvent;
use App\Entity\AwardEvent;
use App\Repository\AwardRepository;
use App\Repository\VenueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use App\Repository\AwardCategoryRepository;

/**
 * Class EditAwardEventHandler
 *
 * @package App\MessageHandler
 */
class EditAwardEventHandler implements MessageHandlerInterface
{
    private $awardEventRepository;
    private $awardRepository;
    private $venueRepository;
    private $awardCategoryRepository;
    private $logger;

    public function __construct(
        AwardEventRepository $awardEventRepository,
        AwardRepository $awardRepository,
        VenueRepository $venueRepository,
        AwardCategoryRepository $awardCategoryRepository,
        LoggerInterface $logger
    ) {
        $this->awardEventRepository = $awardEventRepository;
        $this->awardRepository = $awardRepository;
        $this->venueRepository = $venueRepository;
        $this->awardCategoryRepository = $awardCategoryRepository;
        $this->logger = $logger;
    }

    public function __invoke(EditAwardEvent $message)
    {
        $data = $message->awardEvent();
        $award = $this->awardRepository
            ->find($data['award']);
        $venue = $this->venueRepository
            ->find($data['venue']);
        $categories = $this->awardCategoryRepository
            ->findMultipleById($data['categories']);
        $categoriesArrayCollection = new ArrayCollection($categories);
        $awardEventArray = [
            'id' => $data['id'],
            'name' => $data['name'],
            'award' => $award,
            'venue' => $venue,
            'startDate' => $data['startDate'],
            'endDate' => $data['endDate'],
            'categories' => $categoriesArrayCollection
        ];
        $awardEvent = AwardEvent::toEntity($awardEventArray);
        $this->awardEventRepository
            ->save($awardEvent);
    }
}
