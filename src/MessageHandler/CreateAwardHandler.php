<?php

namespace App\MessageHandler;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Message\CreateAward;
use App\Repository\AwardRepository;
use App\Entity\Award;
use Psr\Log\LoggerInterface;

/**
 * Class CreateAwardHandler
 *
 * @package App\MessageHandler
 */
class CreateAwardHandler implements MessageHandlerInterface
{
    private $repository;
    private $logger;

    public function __construct(AwardRepository $repository, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    public function __invoke(CreateAward $message)
    {
        $award = Award::named($message->awardName());
        $this->repository
            ->save($award);
        $this->logger
            ->info('Created new Award named '.$award->name());
    }
}
