<?php

namespace App\Controller\Admin\Award;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\AwardRepository;

class AwardsController extends AbstractController
{
    private $repository;

    public function __construct(AwardRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke()
    {
        $awards = $this->repository
            ->findAllAsArray();
        
        return $this->render(
            'award/list.html.twig',
            [
                'awards' => $awards
            ]
        );
    }
}
