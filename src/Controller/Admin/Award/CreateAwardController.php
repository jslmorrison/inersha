<?php

namespace App\Controller\Admin\Award;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\AwardType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\CreateAward;

class CreateAwardController extends AbstractController
{
    public function __construct()
    {
    }

    public function __invoke(Request $request, MessageBusInterface $messageBus)
    {
        $form = $this->createForm(AwardType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $messageBus->dispatch(
                    new CreateAward($form->getData()['name'])
                );
                $flashType = 'success';
                $flashMessage = 'Award has been created.';
            } catch (\Throwable $e) {
                $flashType = 'warning';
                $flashMessage = 'Unable to create award.';
            }
            $this->addFlash($flashType, $flashMessage);

            return $this->redirectToRoute('admin_awards_list');
        }

        return $this->render(
            'award/create.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
