<?php

namespace App\Controller\Admin\Award;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\AwardType;
use App\Repository\AwardRepository;
use App\Entity\Award;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\EditAward;

class EditAwardController extends AbstractController
{
    private $repository;

    public function __construct(AwardRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(Request $request, MessageBusInterface $messageBus)
    {
        $award = $this->repository
            ->find($request->get('id'));
        if (!$award instanceof Award) {
            throw new NotFoundHttpException('Invalid award id');
        }

        $form = $this->createForm(AwardType::class, Award::toArray($award));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $messageBus->dispatch(
                    new EditAward(Award::toEntity($form->getData()))
                );
                $flashType = 'success';
                $flashMessage = 'Award has been edited.';
            } catch (\Throwable $e) {
                $flashType = 'warning';
                $flashMessage = 'Unable to edit award.';
            }
            $this->addFlash($flashType, $flashMessage);

            return $this->redirectToRoute('admin_awards_list');
        }

        return $this->render(
            'award/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
