<?php

namespace App\Controller\Admin\AwardEvent;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\AwardEventRepository;

class AwardEventsController extends AbstractController
{
    private $repository;

    public function __construct(AwardEventRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke()
    {
        $awardEvents = $this->repository
            ->findAllAsArray();
        
        return $this->render(
            'award-event/list.html.twig',
            [
                'awardEvents' => $awardEvents
            ]
        );
    }
}
