<?php

namespace App\Controller\Admin\AwardEvent;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\AwardEventRepository;
use App\Entity\AwardEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Form\AwardEventType;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\EditAwardEvent;

class EditAwardEventController extends AbstractController
{
    private $awardEventRepository;

    public function __construct(AwardEventRepository $awardEventRepository)
    {
        $this->awardEventRepository = $awardEventRepository;
    }

    public function __invoke(Request $request, MessageBusInterface $messageBus)
    {
        $awardEvent = $this->awardEventRepository
            ->find($request->get('id'));
        if (!$awardEvent instanceof AwardEvent) {
            throw new NotFoundHttpException('Award Event not found');
        }
        $form = $this->createForm(AwardEventType::class, AwardEvent::toArray($awardEvent));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $messageBus->dispatch(new EditAwardEvent($form->getData()));
                $flashType = 'success';
                $flashMessage = 'Award Event has been edited.';
            } catch (\Throwable $e) {
                $flashType = 'warning';
                $flashMessage = 'Unable to edit Award Event.';
            }
            $this->addFlash($flashType, $flashMessage);

            return $this->redirectToRoute('admin_award_events_list');
        }

        return $this->render(
            'award-event/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
