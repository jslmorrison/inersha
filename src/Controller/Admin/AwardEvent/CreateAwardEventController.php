<?php

namespace App\Controller\Admin\AwardEvent;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\CreateAwardEvent;
use App\Form\AwardEventType;

class CreateAwardEventController extends AbstractController
{
    public function __construct()
    {
    }

    public function __invoke(Request $request, MessageBusInterface $messageBus)
    {
        $form = $this->createForm(AwardEventType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $messageBus->dispatch(new CreateAwardEvent($form->getData()));
                $flashType = 'success';
                $flashMessage = 'Award event has been created.';
            } catch (\Throwable $e) {
                $flashType = 'warning';
                $flashMessage = 'Unable to create award.';
            }
            $this->addFlash($flashType, $flashMessage);

            return $this->redirectToRoute('admin_award_events_list');
        }

        return $this->render(
            'award-event/create.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
