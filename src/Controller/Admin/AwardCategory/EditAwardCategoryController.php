<?php

namespace App\Controller\Admin\AwardCategory;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\AwardCategoryType;
use App\Entity\AwardCategory;
use App\Repository\AwardCategoryRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\EditAwardCategory;

class EditAwardCategoryController extends AbstractController
{
    private $awardCategoryRepository;

    public function __construct(AwardCategoryRepository $awardCategoryRepository)
    {
        $this->awardCategoryRepository = $awardCategoryRepository;
    }

    public function __invoke(Request $request, MessageBusInterface $messageBus)
    {
        $awardCategory = $this->awardCategoryRepository
            ->find($request->get('id'));
        if (!$awardCategory instanceof AwardCategory) {
            throw new NotFoundHttpException('AwardCategory not found');
        }
        $form = $this->createForm(AwardCategoryType::class, AwardCategory::toArray($awardCategory));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $messageBus->dispatch(new EditAwardCategory($form->getData()));
                $flashType = 'success';
                $flashMessage = 'Award Category has been edited.';
            } catch (\Throwable $exception) {
                $flashType = 'warning';
                $flashMessage = 'Unable to edit Award Category.';
            } finally {
                if (isset($exception) && $exception instanceof \Exception) {
                    throw $exception;
                }
                $this->addFlash($flashType, $flashMessage);

                return $this->redirectToRoute('admin_award_categories_list');
            }
        }
        return $this->render(
            'award-category/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
