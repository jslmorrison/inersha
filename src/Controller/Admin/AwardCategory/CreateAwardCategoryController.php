<?php

namespace App\Controller\Admin\AwardCategory;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Form\AwardCategoryType;
use Symfony\Component\Messenger\MessageBusInterface;
use App\Message\CreateAwardCategory;

class CreateAwardCategoryController extends AbstractController
{
    public function __construct()
    {
    }

    public function __invoke(Request $request, MessageBusInterface $messageBus)
    {
        $form = $this->createForm(AwardCategoryType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $messageBus->dispatch(
                    new CreateAwardCategory($form->getData()['name'])
                );
                $flashType = 'success';
                $flashMessage = 'Award Category has been created.';
            } catch (Throwable $e) {
                $flashType = 'warning';
                $flashMessage = 'Unable to create award category.';
            }
            $this->addFlash($flashType, $flashMessage);

            return $this->redirectToRoute('admin_award_categories_list');
        }

        return $this->render(
            'award-category/create.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
