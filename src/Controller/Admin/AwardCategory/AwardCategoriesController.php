<?php

namespace App\Controller\Admin\AwardCategory;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\AwardCategoryDoctrineRepository;
use App\Repository\AwardCategoryRepository;

class AwardCategoriesController extends AbstractController
{
    private $awardcategoryRepository;

    public function __construct(AwardCategoryRepository $awardcategoryRepository)
    {
        $this->awardcategoryRepository = $awardcategoryRepository;
    }

    public function __invoke()
    {
        $awardCategories = $this->awardcategoryRepository
            ->findAllAsArray();
        return $this->render(
            'award-category/list.html.twig',
            [
                'awardCategories' => $awardCategories
            ]
        );
    }
}
