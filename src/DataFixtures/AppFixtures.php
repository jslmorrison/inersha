<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Award;
use Faker;
use App\Entity\AwardCategory;
use App\Entity\Venue;
use App\Entity\AwardEvent;
use Doctrine\Common\Collections\ArrayCollection;

class AppFixtures extends Fixture
{
    private $faker;
    const AWARDNAMES = [
        'Oscars',
        'Razzies',
    ];

    public function load(ObjectManager $manager)
    {
        $this->faker = Faker\Factory::create();
        $this->loadAwardData($manager);
        $this->loadAwardCategoryData($manager);
        $this->loadVenueData($manager);
        $this->loadAwardEventData($manager);
    }

    private function loadAwardData(ObjectManager $manager)
    {
        foreach (self::AWARDNAMES as $name) {
            $award = Award::named($name);
            $manager->persist($award);
            $this->addReference($name.'-award', $award);
        }
        $manager->flush();
    }

    private function loadAwardCategoryData(ObjectManager $manager)
    {
        $categories = [
            'Best Director',
            'Best Actor',
            'Best Cinematography',
            'Worst Director',
            'Worst Actor',
            'Worst Cinematography'
        ];
        foreach ($categories as $category) {
            $awardCategory = AwardCategory::named($category);
            $manager->persist($awardCategory);
            $this->addReference($category, $awardCategory);
        }
        $manager->flush();
    }

    private function loadVenueData(ObjectManager $manager)
    {
        $venue = Venue::nameAndAddress(
            'Chans Chinese Theatre',
            'Broadway',
            'London',
            'uk1 1uk',
            'GB'
        );
        $manager->persist($venue);
        $this->addReference('chansVenue', $venue);
        $manager->flush();
    }

    private function loadAwardEventData(ObjectManager $manager)
    {
        $awardEvent = AwardEvent::create(
            'Oscars - '.date('Y'),
            $this->getReference('Oscars-award'),
            $this->getReference('chansVenue'),
            new \DateTime(),
            new \DateTime(),
            new ArrayCollection(
                [
                    $this->getReference('Best Director'),
                    $this->getReference('Best Actor'),
                    $this->getReference('Best Cinematography')
                ]
            )
        );
        $manager->persist($awardEvent);
        $manager->flush();
    }
}
