<?php

namespace App\Message;

/**
 * Class CreateAwardCategory
 *
 * @packahe App\Message
 */
class CreateAwardCategory
{
    private $categoryName;

    public function __construct(string $categoryName)
    {
        $this->categoryName = $categoryName;
    }

    public function categoryName(): string
    {
        return $this->categoryName;
    }
}
