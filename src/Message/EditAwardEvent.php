<?php

 namespace App\Message;

/**
 * Class EditAwardEvent
 *
 * @package App\Message
 */
class EditAwardEvent
{
    private $awardEvent;

    public function __construct(array $awardEvent)
    {
        $this->awardEvent = $awardEvent;
    }

    public function awardEvent(): array
    {
        return $this->awardEvent;
    }
}
