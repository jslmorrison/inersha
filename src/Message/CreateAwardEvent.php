<?php

namespace App\Message;

/**
 * Class CreateAward
 *
 * @package App\Message
 */
class CreateAwardEvent
{
    private $info;

    public function __construct(array $info)
    {
        $this->info = $info;
    }

    public function info(): array
    {
        return $this->info;
    }
}
