<?php

namespace App\Message;

use App\Entity\Award;

class EditAward
{
    private $award;

    public function __construct(Award $award)
    {
        $this->award = $award;
    }

    public function award(): Award
    {
        return $this->award;
    }
}
