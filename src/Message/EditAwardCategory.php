<?php

namespace App\Message;

/**
 * Class EditAwardCategory
 *
 * @package App\Message
 */
class EditAwardCategory
{
    private $awardCategory;

    public function __construct(array $awardCategory)
    {
        $this->awardCategory = $awardCategory;
    }

    public function awardCategory(): array
    {
        return $this->awardCategory;
    }
}
