<?php

namespace App\Message;

/**
 * Class CreateAward
 *
 * @package App\Message
 */
class CreateAward
{
    private $awardName;

    public function __construct(string $awardName)
    {
        $this->awardName = $awardName;
    }

    public function awardName(): string
    {
        return $this->awardName;
    }
}
