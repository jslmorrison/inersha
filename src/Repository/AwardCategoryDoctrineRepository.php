<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\AwardCategory;

/**
 * Class AwardCategoryDoctrineRepository
 *
 * @package App\Repository
 */
class AwardCategoryDoctrineRepository extends ServiceEntityRepository implements AwardCategoryRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AwardCategory::class);
    }

    public function findAllAsArray()
    {
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('ac')
            ->from($this->getEntityName(), 'ac')
            ->orderBy('ac.name', 'asc');
        
        return $queryBuilder->getQuery()
            ->getArrayResult();
    }

    public function findAllAsChoices(): array
    {
        $choices = [];
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('ac')
            ->from($this->getEntityName(), 'ac')
            ->orderBy('ac.name', 'asc');
        $categories = $queryBuilder->getQuery()
            ->getArrayResult();
        foreach ($categories as $category) {
            $choices[$category['name']] = $category['id'];
        }

        return $choices;
    }

    public function save(AwardCategory $awardCategory)
    {
        $this->_em
            ->merge($awardCategory);
        $this->_em
            ->flush();
    }

    public function findMultipleById(array $ids)
    {
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('ac')
            ->from($this->getEntityName(), 'ac');
        $queryBuilder->where($queryBuilder->expr()->in('ac.id', $ids));

        return $queryBuilder->getQuery()
            ->getResult();
    }
}
