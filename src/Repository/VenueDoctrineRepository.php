<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Venue;

/**
 * Class VenueDoctrineRepository
 *
 * @package App\Entity
 */
class VenueDoctrineRepository extends ServiceEntityRepository implements VenueRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Venue::class);
    }

    public function findAllAsChoices(): array
    {
        $choices = [];
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('v')
            ->from($this->getEntityName(), 'v')
            ->orderBy('v.name', 'asc')
            ->addOrderBy('v.town', 'asc');
        $venues = $queryBuilder->getQuery()
            ->getArrayResult();
        foreach ($venues as $venue) {
            $choices[$venue['name'].', '.$venue['town']] = $venue['id'];
        }

        return $choices;
    }
}
