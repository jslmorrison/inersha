<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Award;

/**
 * Class AwardDoctrineRepository
 *
 * @package App\Repository
 */
class AwardDoctrineRepository extends ServiceEntityRepository implements AwardRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Award::class);
    }

    public function findAllAsArray(): array
    {
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder();
        $queryBuilder->select('a')
            ->from($this->getEntityName(), 'a')
            ->orderBy('a.name', 'asc')
            ->addOrderBy('a.created', 'desc');
        
        return $queryBuilder->getQuery()
            ->getArrayResult();
    }

    public function save(Award $award)
    {
        $this->_em
            ->merge($award);
        $this->_em
            ->flush();
    }

    public function findAllAsChoices(): array
    {
        $choices = [];
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from($this->getEntityName(), 'a')
            ->orderBy('a.name', 'asc');
        $awards = $queryBuilder->getQuery()
            ->getArrayResult();
        foreach ($awards as $award) {
            $choices[$award['name']] = $award['id'];
        }

        return $choices;
    }
}
