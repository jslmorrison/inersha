<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use App\Entity\AwardEvent;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class AwardEventDoctrineRepository
 *
 * @package App\Repository
 */
class AwardEventDoctrineRepository extends ServiceEntityRepository implements AwardEventRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AwardEvent::class);
    }

    public function save(AwardEvent $awardEvent)
    {
        $this->_em
            ->merge($awardEvent);
        $this->_em
            ->flush();
    }

    public function findAllAsArray(): array
    {
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('ae', 'v')
            ->from($this->getEntityName(), 'ae')
            ->leftJoin('ae.venue', 'v')
            ->orderBy('ae.startDate', 'desc');
        
        return $queryBuilder->getQuery()
            ->getArrayResult();
    }
}
